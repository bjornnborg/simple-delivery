Rails.application.routes.draw do
  resources :maps do
    resources :segments
  end

  get  "maps/:id/routing", to: "maps#start_routing", as: :start_routing
  post "maps/:id/routing", to: "maps#do_routing", as: :do_routing

  root to: 'maps#index'
end
