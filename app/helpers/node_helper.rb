# Translate business models to the abstraction of the problem
module NodeHelper

  def self.from_segments(segments)
    nodes = []
    return nodes unless segments

    nodes_hash = Hash.new
    segments.each do |s|
      source = nodes_hash[s.source]
      if !source
        source = Graph::Node.new(s.source)
        nodes_hash[s.source] = source
      end
        
      destination = nodes_hash[s.destination]
      if !destination
        destination = Graph::Node.new(s.destination)
        nodes_hash[s.destination] = destination
      end

      source_edge = Graph::Edge.new(destination, s.distance)
      source.edges << source_edge

      destination_edge = Graph::Edge.new(source, s.distance)
      destination.edges << destination_edge
    end

    nodes_hash.values
  end

end
