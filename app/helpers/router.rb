# High-level abstraction to find route information based on the given parameters.
# This is the entry point for the clients of the api
module Router

  def self.route(map, routing_info)
    nodes = NodeHelper.from_segments(map.segments)
    starting_node = nodes.select{|n| n.name == routing_info[:from]}.first
    route = starting_node.path_to(routing_info[:to])
    RouteInfo.new(route, routing_info[:fuel_consumption], routing_info[:fuel_price])
  end

end
