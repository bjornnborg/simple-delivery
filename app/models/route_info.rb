# High-level abstraction of the result of the routing computation.
# Knows the steps and distance to reach the destination point.
# Also knows information about the amount needed to take the given way.
# This is the return type to the clients of the api
class RouteInfo
  attr_accessor :route, :cost

  def initialize(route, fuel_consumption, fuel_price)
    @route = route
    @cost = (route.distance / fuel_consumption.to_d) * fuel_price.to_d # to_d because I decided to pass in the request hash
  end

end
