# Classes in the graph module are a abstraction of the problem.
# They intend to map business objects to the solution concept.
# This class represents a node, which will be connect to another node by an edge.
# A Segment becames 2 nodes connected by an Edge
module Graph
  class Node
    attr_accessor :name, :edges

    def initialize(name, edges=[])
      @name = name
      @edges = edges
    end

    # Return a route object with the shortest path do the given node
    def path_to(node_name)
      nodes = []
      return nodes unless edges.any?
      nodes = traverse_edges(self, node_name, [])
    end

    def sorted_edges
      @sorted ||= @edges.sort {|a,b| a.distance <=> b.distance}
      @sorted
    end

    def to_s
      @name
    end

    private
    # Traverses nodes recursively in order to find the shortest path.
    # This method assumes all nodes are connected in some way (i.e. Cyclic tree)
    # It uses some of Dijkstra's premisses and always get the shortest edge from
    # a given node. By doing this we can reach the shortest path by visting each
    # node only once, if needed, so we can get O(log) complexity.
    # The first approach was a depth first implementation, but was aborted because
    # it was 'underteministic': finding a element didn't mean you find it by the
    # shortest path. So, it was necessary to iterate all nodes in the graph, which
    # led to a O(n) complexity.
    def traverse_edges(node, node_name, visited_nodes)
      return Route.new if visited_nodes.include? node
      visited_nodes << node
      route = Route.new
      edge = find_nearest_non_visited_edge(node, visited_nodes)

      raise "No more edges to search. Looks like you are on a dead end" unless edge
      if edge.node.name == node_name
        route.add_edge(edge)
      else
        route += traverse_edges(edge.node, node_name, visited_nodes)
        route.distance += edge.distance
      end

      route.nodes.unshift(node) if route.nodes.any?
      route
    end

    def find_nearest_non_visited_edge(node, visited_nodes)
      node.sorted_edges.reject{|e| visited_nodes.include?(e.node)}.first
    end

  end
end
