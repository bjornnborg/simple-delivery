# Represents a connection between 2 nodes (i.e. source and destination)
# An edge knows the distance between the nodes and could be used to store
# other information. In the context of this business it could store toil 
# and road infomation, for example.
module Graph
  class Edge
    attr_accessor :distance, :node

    def initialize(node, distance)
      @node = node
      @distance = distance
    end
  end
end
