# Represent a set of nodes to be traversed in order to reach a destination.
# Instances of this class are created by the component responsible to find
# the shortest path between 2 nodes
module Graph
  class Route
    attr_accessor :nodes, :distance

    def initialize(nodes=[], distance=0)
      @nodes = nodes
      @distance = distance  
    end

    def add_edge(edge)
      @nodes << edge.node
      @distance += edge.distance
    end

    def +(route)
      Route.new(@nodes + route.nodes, @distance + route.distance)
    end

    def to_s
      nodes.map{|n| n.name}.join(", ")
    end

    def to_a
      nodes.map{|n| n.name}
    end    

  end
end
