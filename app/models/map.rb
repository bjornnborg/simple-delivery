# Represents a custom map, which will have segments/steps/nodes
class Map < ActiveRecord::Base
  has_many :segments
end
