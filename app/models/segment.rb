# Represents a connection between a source and a destination
class Segment < ActiveRecord::Base
  belongs_to :map
end
