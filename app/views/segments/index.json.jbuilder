json.array!(@segments) do |segment|
  json.extract! segment, :id, :source, :destination, :distance, :map_id
  json.url segment_url(segment, format: :json)
end
