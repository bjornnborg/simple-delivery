# By this controller you can get routes from a map.
class MapsController < ApplicationController
  #include Roar::Rails::ControllerAdditions
  #include Roar::Rails::ControllerAdditions::Render
  # Comments above was a try to use ROAR in order to get HATEOAS behavior.
  # The goal was to make resources 'discoverable'
  # Got some problems and couldn't get it to work in time.

  before_action :set_map, only: [:show, :edit, :update, :destroy, :start_routing, :do_routing]

  # Should have splited routing in another controller/resource.
  # It would give me more flexibility and would make this controller
  # less bloated
  def start_routing
    @starting_points = @map.segments.map{|s| [s.source, s.source]}
    @destination_points = @map.segments.map{|s| [s.destination, s.destination]}
  end

  def do_routing
    # Only responds to html/template. 
    # Got stack overflow when serializing as json. Looks like some problem with
    # the serializers, once it doesn't occur with the same model anywhere.
    @routing_info = Router.route(@map, build_routing_info(params))
  end 

  def index
    @maps = Map.all
  end

  def show
  end

  def new
    @map = Map.new
  end

  def edit
  end

  def create
    @map = Map.new(map_params)

    respond_to do |format|
      if @map.save
        format.html { redirect_to @map, notice: 'Map was successfully created.' }
        format.json { render :show, status: :created, location: @map }
      else
        format.html { render :new }
        format.json { render json: @map.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @map.update(map_params)
        format.html { redirect_to @map, notice: 'Map was successfully updated.' }
        format.json { render :show, status: :ok, location: @map }
      else
        format.html { render :edit }
        format.json { render json: @map.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @map.destroy
    respond_to do |format|
      format.html { redirect_to maps_url, notice: 'Map was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_map
      @map = Map.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def map_params
      params.require(:map).permit(:name)
    end

    def build_routing_info(params)
      params.slice(:from, :to, :fuel_consumption, :fuel_price)
    end
end
