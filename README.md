# Simple Delivery Routing
[![Build Status](https://semaphoreci.com/api/v1/projects/bbfeb8af-412d-46bc-9f7d-ba97d8d44d96/487508/badge.svg)](https://semaphoreci.com/bjornnborg/simple-delivery)
[![Code Climate](https://codeclimate.com/repos/55ac880ae30ba0412401e8e5/badges/399b5f35ffb4a2dc0c8a/gpa.svg)](https://codeclimate.com/repos/55ac880ae30ba0412401e8e5/feed)
[![Test Coverage](https://codeclimate.com/repos/55ac880ae30ba0412401e8e5/badges/399b5f35ffb4a2dc0c8a/coverage.svg)](https://codeclimate.com/repos/55ac880ae30ba0412401e8e5/coverage)

Find the shortest path between two points. Given a map with segments in the following format:

- A B 10
- B D 15
- A C 20
- C D 30
- B E 50
- D E 30

returns A,B,D as the path to be covered.

It also gets some information about fuel consumption and cost in order to say the total cost of the chosen path.

# Technical Information
It was developed using Ruby on Rails and is deployed on heroku: https://simple-delivery-routing.herokuapp.com/

It was intended to get a RESTFul level 3 API with HATEOAS, althought it is incomplete for now.

In order to use the application you can access the link above and create some maps and segments to compute routes.

You can also run the tests to discover more about the application's behavior:

```ruby
bundle exec rspec -fd
```

