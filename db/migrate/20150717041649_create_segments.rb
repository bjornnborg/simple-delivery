class CreateSegments < ActiveRecord::Migration
  def change
    create_table :segments do |t|
      t.string :source
      t.string :destination
      t.decimal :distance, precision: 5, scale: 1
      t.references :map, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
