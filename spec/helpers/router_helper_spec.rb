require 'rails_helper'
require 'router'

include Router

RSpec.describe Router, :type => :helper do

  context "Routing" do

    it "must find route starting from first node" do
      map = Map.new
      map.name = "The one"

      a_b = Segment.new
      a_b.source = "A"
      a_b.destination = "B"
      a_b.distance = 35

      b_c = Segment.new
      b_c.source = "B"
      b_c.destination = "C"
      b_c.distance = 45

      map.segments = [a_b, b_c]

      route_info = Router.route(map, from: "A", to: "C", fuel_consumption: 10, fuel_price: 2.37)
      expect(route_info.route.to_s).to eq "A, B, C"
      expect(route_info.route.distance).to eq 80
      expect(route_info.cost).to eq 18.96
    end

    it "must solve the problem stated in the email" do
      #     E
      #    / \
      #   B - D
      #   |   |
      #   A - C

      map = Map.new
      map.name = "The one"

      a_b = Segment.new
      a_b.source = "A"
      a_b.destination = "B"
      a_b.distance = 10

      a_c = Segment.new
      a_c.source = "A"
      a_c.destination = "C"
      a_c.distance = 20      

      b_d = Segment.new
      b_d.source = "B"
      b_d.destination = "D"
      b_d.distance = 15

      b_e = Segment.new
      b_e.source = "B"
      b_e.destination = "E"
      b_e.distance = 50

      c_d = Segment.new
      c_d.source = "C"
      c_d.destination = "D"
      c_d.distance = 30

      d_e = Segment.new
      d_e.source = "D"
      d_e.destination = "E"
      d_e.distance = 30

      map.segments = [a_b, a_c, b_d, b_e, c_d, d_e]

      route_info = Router.route(map, from: "A", to: "D", fuel_consumption: 10, fuel_price: 2.50)
      expect(route_info.route.to_s).to eq "A, B, D"
      expect(route_info.route.distance).to eq 25
      expect(route_info.cost).to eq 6.25
    end

    it "must solve the problem stated in the email, backwards" do
      #just to be sure we can pick the correct node before start traversing
      #     E
      #    / \
      #   B - D
      #   |   |
      #   A - C      
      map = Map.new
      map.name = "The one"

      a_b = Segment.new
      a_b.source = "A"
      a_b.destination = "B"
      a_b.distance = 10

      a_c = Segment.new
      a_c.source = "A"
      a_c.destination = "C"
      a_c.distance = 20      

      b_d = Segment.new
      b_d.source = "B"
      b_d.destination = "D"
      b_d.distance = 15

      b_e = Segment.new
      b_e.source = "B"
      b_e.destination = "E"
      b_e.distance = 50

      c_d = Segment.new
      c_d.source = "C"
      c_d.destination = "D"
      c_d.distance = 30

      d_e = Segment.new
      d_e.source = "D"
      d_e.destination = "E"
      d_e.distance = 30

      map.segments = [a_b, a_c, b_d, b_e, c_d, d_e]

      route_info = Router.route(map, from: "D", to: "A", fuel_consumption: 10, fuel_price: 2.50)
      expect(route_info.route.to_s).to eq "D, B, A"
      expect(route_info.route.distance).to eq 25
      expect(route_info.cost).to eq 6.25
    end    

  end

end
