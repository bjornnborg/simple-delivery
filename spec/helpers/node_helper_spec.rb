require 'rails_helper'
require 'node_helper'

include NodeHelper

RSpec.describe NodeHelper, :type => :helper do
  context "Transform segment to nodes" do
    it "must return no nodes if no segment is given" do
      nodes = NodeHelper.from_segments([])
      expect(nodes.size).to eq 0
    end

    it "must convert 1 segment in 2 nodes with 1 edge each" do
      segment = Segment.new
      segment.source = "A"
      segment.destination = "B"
      segment.distance = 10

      nodes = NodeHelper.from_segments([segment])

      expect(nodes.size).to eq 2

      expect(nodes.first.name).to eq segment.source
      expect(nodes.first.edges.size).to eq 1
      expect(nodes.first.edges[0].distance).to eq segment.distance
      expect(nodes.first.edges[0].node.name).to eq segment.destination

      expect(nodes.last.name).to eq segment.destination
      expect(nodes.last.edges.size).to eq 1
      expect(nodes.last.edges[0].distance).to eq segment.distance
      expect(nodes.last.edges[0].node.name).to eq segment.source
    end

    it "must convert 2 segment in 4 nodes with 1 edge each" do
      segment_one = Segment.new
      segment_one.source = "A"
      segment_one.destination = "B"
      segment_one.distance = 10

      segment_two = Segment.new
      segment_two.source = "C"
      segment_two.destination = "D"
      segment_two.distance = 20

      nodes = NodeHelper.from_segments([segment_one, segment_two])

      expect(nodes.size).to eq 4

      expect(nodes[0].name).to eq segment_one.source
      expect(nodes[0].edges.size).to eq 1

      expect(nodes[1].name).to eq segment_one.destination
      expect(nodes[1].edges.size).to eq 1

      expect(nodes[2].name).to eq segment_two.source
      expect(nodes[2].edges.size).to eq 1

      expect(nodes[3].name).to eq segment_two.destination
      expect(nodes[3].edges.size).to eq 1      
    end
  end

  context "Correctly assing multiple edges for a node" do
    it "must assing multiple edges to a node" do
      segment_one = Segment.new
      segment_one.source = "A"
      segment_one.destination = "B"
      segment_one.distance = 10

      segment_two = Segment.new
      segment_two.source = "A"
      segment_two.destination = "C"
      segment_two.distance = 20

      nodes = NodeHelper.from_segments([segment_one, segment_two])

      expect(nodes.size).to eq 3

      expect(nodes[0].name).to eq "A"
      expect(nodes[0].edges.size).to eq 2
      expect(nodes[0].edges[0].node.name).to eq "B"
      expect(nodes[0].edges[0].distance).to eq 10
      expect(nodes[0].edges[1].node.name).to eq "C"
      expect(nodes[0].edges[1].distance).to eq 20      

    end
  end

end
