require 'spec_helper'
require 'rails_helper'

module Graph
  RSpec.describe Node, :type => :model do

    context "Find straight paths" do
      it "must find simple straight path from a node with only 1 edge" do
        # A - B
        starting_node = Graph::Node.new("A")
        ending_node = Graph::Node.new("B")

        starting_node.edges=[Graph::Edge.new(ending_node, 10)]
        ending_node.edges=[Graph::Edge.new(starting_node, 10)]

        route = starting_node.path_to("B")

        expect(route.to_s).to eq "A, B"
        expect(route.distance).to eq 10
      end


    end

    context "Find traversal paths" do
      it "must traverse edges in a straight path" do
        # A - B - C
        starting_node = Graph::Node.new("A")
        middle_node = Graph::Node.new("B")
        ending_node = Graph::Node.new("C")

        starting_node.edges=[Graph::Edge.new(middle_node, 10)]

        middle_node.edges ||= [Graph::Edge.new(starting_node, 10)]
        middle_node.edges << Graph::Edge.new(ending_node, 10)

        ending_node.edges=[Graph::Edge.new(middle_node, 10)]      

        route = starting_node.path_to("C")

        expect(route.to_s).to eq "A, B, C"
        expect(route.distance).to eq 20
      end



    end

    context "Distance calculations" do
      it "must choose the shortest path" do
        #   B
        #  / \
        # A   D
        #  \ /
        #   C
        node_a = Graph::Node.new("A")
        node_b = Graph::Node.new("B")
        node_c = Graph::Node.new("C")
        node_d = Graph::Node.new("D")

        node_a.edges = [Graph::Edge.new(node_b, 50)]
        node_a.edges << Graph::Edge.new(node_c, 10) #shortest path

        node_b.edges = [Graph::Edge.new(node_a, 50)]
        node_b.edges << Graph::Edge.new(node_d, 30)

        node_c.edges = [Graph::Edge.new(node_a, 10)]
        node_c.edges << Graph::Edge.new(node_d, 20) #shortes path

        route = node_a.path_to("D")

        expect(route.to_a).to eq ["A", "C", "D"]
        expect(route.distance).to eq 30
      end
    end

    context "Dead ends" do
      it "must raise exeception if cannot find given node" do
        #   B
        #  /
        # A
        #  \
        #   C
        starting_node = Graph::Node.new("A")
        left_node = Graph::Node.new("B")
        right_node = Graph::Node.new("C")

        starting_node.edges = []
        starting_node.edges << Graph::Edge.new(left_node, 10)
        starting_node.edges << Graph::Edge.new(right_node, 10)

        left_node.edges=[Graph::Edge.new(starting_node, 10)]
        right_node.edges=[Graph::Edge.new(starting_node, 10)]

        # Cannot find "C" if it is away or in the same distance as "B".
        # In both cases, "B" will be chosen first and will led to a dead end
        # In a depth first search, it would be possible to pop "B" from the
        # stack and search on the other side
        expect {starting_node.path_to("C")}.to raise_exception(/dead end/)
      end

      it "must raise execption on traversing edges if cannot find given node" do
        #   B
        #  /
        # A   D
        #  \ /
        #   C
        #    \
        #     E
        node_a = Graph::Node.new("A")
        node_b = Graph::Node.new("B")
        node_c = Graph::Node.new("C")
        node_d = Graph::Node.new("D")
        node_e = Graph::Node.new("E")

        node_a.edges = [Graph::Edge.new(node_b, 10)]
        node_a.edges << Graph::Edge.new(node_c, 10)

        node_b.edges = [Graph::Edge.new(node_a, 10)]

        node_c.edges = [Graph::Edge.new(node_a, 10)]
        node_c.edges << Graph::Edge.new(node_d, 10)
        node_c.edges << Graph::Edge.new(node_e, 10)
        
        # Cannot find "C" if it is away or in the same distance as "B".
        # In both cases, "B" will be chosen first and will led to a dead end
        # In a depth first search, it would be possible to pop "B" from the
        # stack and search on the other side        
        expect{node_a.path_to("E")}.to raise_exception(/dead end/)

      end    

    end

  end
end
