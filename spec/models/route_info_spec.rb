require 'spec_helper'
require 'rails_helper'

RSpec.describe RouteInfo, :type => :model do
  context "Compute data" do
    it "must compute data correctly" do
      route = Graph::Route.new([], 95)
      route_info = RouteInfo.new(route, 10, 2.5)
      expect(route_info.cost).to eq 23.75
    end
  end
end
